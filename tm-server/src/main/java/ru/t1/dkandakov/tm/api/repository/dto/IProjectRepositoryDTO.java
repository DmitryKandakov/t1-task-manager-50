package ru.t1.dkandakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;

import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnerRepositoryDTO<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull ProjectSort sort);

}
