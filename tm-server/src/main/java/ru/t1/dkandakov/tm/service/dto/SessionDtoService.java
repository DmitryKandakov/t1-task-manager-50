package ru.t1.dkandakov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.dkandakov.tm.api.service.IConnectionService;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;
import ru.t1.dkandakov.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionRepositoryDTO> implements ISessionServiceDTO {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ISessionRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
